//
//  CatalogCollectionViewCell.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 05/08/17.
//
//

import UIKit

class CatalogCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var year: UILabel!

    override var isHighlighted: Bool {
        didSet {
            alpha = isHighlighted ? 0.7 : 1
        }
    }
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor(r: 209, g: 209, b: 209) : UIColor(r: 240, g: 240, b: 240)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layer.cornerRadius = 4
        layer.borderWidth = 1
        layer.borderColor = backgroundColor?.cgColor
        layer.backgroundColor = backgroundColor?.cgColor
    }
}
