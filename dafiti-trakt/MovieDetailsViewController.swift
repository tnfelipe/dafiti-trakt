//
//  MovieDetailsViewController.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 05/08/17.
//
//

import UIKit

protocol MovieDetailsDelegate {
    func updateMovieInfo(_ movie: Movie)
}

class MovieDetailsViewController: UIViewController {
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var posterView: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var runtime: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var genres: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!

    open var movie: Movie!
    open var delegate: MovieDetailsDelegate?
    private var imagesOverlay: CAShapeLayer?

    // MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitle()

        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = UIColor.white

        guard movie != nil else {
            alert(message: "Something went wrong.\nPlease try again.", completionHandler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
            return
        }

        //posterView.image = movie.poster?.image; movie.posterView = posterView
        movieName.text = movie.title
        releaseDate.text = movie.year
        movie.getPoster(addedTo: posterView)

        // load extended info
        if movie.expanded {
            fillExtendedInfo()
        } else {
            loadExntendedInfo()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ImageDisplayViewController {
            guard let selectedRow = collectionView.indexPathsForSelectedItems?.first?.row else { return }
            vc.image = movie.images[selectedRow].image!
        }
    }

    // MARK: - Actions
    private func fillExtendedInfo() {
        guard movie.expanded else {
            loadExntendedInfo()
            return
        }

        //delegate?.updateMovieInfo(movie)
        runtime.text = String(describing: movie.runtime ?? 0)
        tagline.text = movie.tagline
        overview.text = movie.overview
        rating.text = String(format: "%.2f", movie.rating ?? 0)
        genres.text = movie.genres.joined(separator: ", ")
        favButton.setImage(movie.favored ? UIImage(named: "trakt-icon") : UIImage(named: "trakt-icon-off"), for: .normal)

        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        releaseDate.text = movie.releaseDate != nil ? format.string(from: movie.releaseDate!) : "-"
    }

    private func loadExntendedInfo() {
        let loadingOverlay = view.showLoading()
        imagesOverlay = collectionView.showLoading()

        let req = MovieExtendedRequest(movie: movie)
        req.request { (response: MovieExtendedResponse) in

            // pra não precisar chamar de novo
            let imagesResponse = self.movie.imagesResponse

            self.movie = response.extendedMovie
            self.movie.imagesResponse = imagesResponse

            DispatchQueue.main.async {
                self.fillExtendedInfo()
                loadingOverlay.removeFromSuperlayer()

                self.movie.loadImages {
                    self.collectionView.reloadData()
                    self.imagesOverlay?.removeFromSuperlayer()
                }
            }
        }
    }

    @IBAction func tappedFav(_ sender: Any) {
        let favoring = !movie.favored
        var newFavorites: [String] = favoring ? [movie.id.trakt] : []
        if let favorites = UserDefaults.standard.array(forKey: "dafiti-trakt-favorites") as? [String] {
            if favoring {
                newFavorites.append(contentsOf: favorites)
            } else {
                newFavorites.append(contentsOf: favorites.filter({ (favId) -> Bool in
                    favId != movie.id.trakt
                }))
            }
        }

        movie.favored = favoring
        favButton.setImage(favoring ? UIImage(named: "trakt-icon") : UIImage(named: "trakt-icon-off"), for: .normal)
        UserDefaults.standard.set(newFavorites, forKey: "dafiti-trakt-favorites")
    }
}

extension MovieDetailsViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movie.images.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath)
        guard let galleryCell = cell as? GalleryCollectionViewCell else { return cell }

        guard let image = movie.images[indexPath.row].image else { return galleryCell }
        galleryCell.imageView.image = image
        galleryCell.lcImageViewHeight.constant = min(image.size.height / 2, 85)

        return galleryCell
    }
}

extension MovieDetailsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showImage", sender: self)
    }
}
