//
//  Movie.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 04/08/17.
//
//

import UIKit

class MovieImage: Hashable {
    enum ImageStatus: UInt {
        case notLoaded = 0, loading, loaded, loadingError
    }

    var url: String!
    var size: String!
    var image: UIImage?
    var dtError: DTError?
    var status: ImageStatus! {
        didSet {
            guard waiters.count > 0 else { return }

            waiters.forEach { (waiter) in
                if self.status == .loaded, let imageView = waiter.imageView {
                    DispatchQueue.main.async {
                        waiter.overlay?.removeFromSuperlayer()
                        imageView.image = self.image
                    }
                }
                waiter.handler?(self, self.dtError)
            }
            waiters = []
        }
    }

    var hashValue: Int {
        return url.hashValue
    }

    /// onde enviar após carregar
    struct ImageWaiter {
        var imageView: UIImageView?
        var overlay: CAShapeLayer?
        var handler: ((MovieImage, DTError?) -> Void)?
    }
    var waiters: [ImageWaiter] = []

    init(url: String, size: String = "", image: UIImage? = nil) {
        self.url = url
        self.size = size
        self.image = image

        if image != nil {
            status = .loaded
        } else {
            status = .notLoaded
        }
    }

    func getPoster(addedTo imageView: UIImageView, completionHandler: ((MovieImage, DTError?) -> Void)? = nil) {
        if status == .loaded {
            DispatchQueue.main.async { imageView.image = self.image }
            completionHandler?(self, dtError)
            return
        }

        var overlay: CAShapeLayer?
        DispatchQueue.main.async { overlay = imageView.showLoading() }

        if status == .loading {
            self.waiters.append(ImageWaiter(imageView: imageView, overlay: overlay, handler: completionHandler))
            return
        }


        Client.getImage(url, withSize: size) { (loadedImage: UIImage?, error) in
            DispatchQueue.main.async {
                overlay?.removeFromSuperlayer()
                overlay = nil
            }

            guard loadedImage != nil, error == nil else {
                self.dtError = error
                self.status = .loadingError
                completionHandler?(self, error)
                return
            }

            self.image = loadedImage
            self.status = .loaded
            DispatchQueue.main.async { imageView.image = self.image }
            completionHandler?(self, nil)
        }
    }
}

extension MovieImage: Equatable {
    static func ==(lhs: MovieImage, rhs: MovieImage) -> Bool {
        return lhs.url == rhs.url
    }
}

class Movie {
    struct MovieID {
        var trakt: String
        var tmdb: String

        init(trakt: String, tmdb: String) {
            self.trakt = trakt
            self.tmdb = tmdb
        }

        init(fields: [String: Any]?) {
            self.trakt = String(describing: fields?["trakt"] ?? "")
            self.tmdb = String(describing: fields?["tmdb"] ?? "")
        }
    }

    var id: MovieID!
    var title: String!
    var poster: MovieImage?
    var year: String!
    var favored: Bool = false

    var expanded: Bool!
    var releaseDate: Date?
    var runtime: UInt!
    var tagline: String!
    var overview: String!
    var rating: Double!
    var genres: [String]!
    var images: [MovieImage]!
    var imagesResponse: TMDBMovieImagesResponse?

    init(
        id: Movie.MovieID,
        title: String,
        poster: MovieImage?,
        year: String,
        expanded: Bool = false,
        releaseDate: Date? = nil,
        runtime: UInt = 0,
        tagline: String = "",
        overview: String = "",
        rating: Double = 0,
        genres: [String] = []) {

        self.id = id
        self.title = title
        self.poster = poster
        self.year = year
        self.favored = false

        if let favorites = UserDefaults.standard.array(forKey: "dafiti-trakt-favorites") as? [String] {
            self.favored = favorites.contains(id.trakt)
        }

        self.expanded = expanded
        self.releaseDate = releaseDate ?? Date()
        self.runtime = runtime
        self.tagline = tagline
        self.overview = overview
        self.rating = rating
        self.genres = genres
        self.images = []
        //self.imagesLoadedHandler = nil

        // já carregar os endereços das imagens
        guard imagesResponse == nil else { return }
        DispatchQueue.main.async {
            let imagesReq = TMDBMovieImagesRequest(id: self.id)
            imagesReq.request(completionHandler: { (response: TMDBMovieImagesResponse) in
                self.imagesResponse = response // deixa salvo para não precisar chamar de novo se for carregar galeria
                self.poster = MovieImage(url: response.posterUrl)
            })
        }
    }

    convenience init(fields: [String: Any]) {
        let ids = MovieID(fields: fields["ids"] as? [String: Any])
        self.init(
            id: ids,
            title: fields["title"] as? String ?? "unkown",
            poster: nil,
            year: String(describing: fields["year"] ?? "-"))
    }

    open func getPoster(addedTo imageView: UIImageView, completionHandler: ((MovieImage, DTError?) -> Void)? = nil) {
        DispatchQueue.main.async { imageView.image = nil }

        // não carregou as imagens do filme ainda
        guard let poster = self.poster else {
            var overlay: CAShapeLayer?
            DispatchQueue.main.async { overlay = imageView.showLoading() }

            DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
                DispatchQueue.main.async {
                    overlay?.removeFromSuperlayer()
                    overlay = nil
                }

                self.getPoster(addedTo: imageView, completionHandler: completionHandler)
            })
            return
        }

        if poster.status == .loaded {
            DispatchQueue.main.async { imageView.image = poster.image }
            completionHandler?(poster, nil)
            return
        }

        poster.getPoster(addedTo: imageView, completionHandler: completionHandler)
    }

    open func loadImages(completionHandler: @escaping () -> Void) {
        guard imagesResponse != nil else { return }

        var total = imagesResponse!.backdrops.count
        for image in imagesResponse!.backdrops {
            let size = tmdbConfiguration!.imagesSizes.first ?? "w300"

            Client.getImage(image.url, withSize: size, completionHandler: { (newImage, error) in
                guard error == nil, let imageData = newImage else {
                    total -= 1
                    return
                }
                DispatchQueue.main.async {
                    self.images.append(MovieImage(url: image.url, image: imageData))
                    if self.images.count == total {
                        completionHandler()
                    }
                }
            })
        }
    }
}

extension Movie: Equatable {
    static func ==(lhs: Movie, rhs: Movie) -> Bool {
        return lhs.id.trakt == rhs.id.trakt
    }
}


// MARK: - Popular Movies
class PopularMoviesRequest: Request {
    required override init() {
        super.init()

        api = .trakt
        path = "movies/popular"
    }

    convenience init(page: Int = 1, limit: Int = 10, query: String = "") {
        self.init()

        path = path + "?page=\(page)&limit=\(limit)&query=\(query)"
    }
}

class PopularMoviesResponse: Response {
    open var movies: [Movie] = []
    open var page: Int = 0
    open var pages: Int = 0
    open var limit: Int = 0

    override func map() {
        guard let jsonMovies = jsonFields else { return }
        jsonMovies.forEach { (jsonMovie) in
            movies.append(Movie(fields: jsonMovie))
        }

        page = Int(headers?["x-pagination-page"] as? String ?? "0")!
        pages = Int(headers?["x-pagination-page-count"] as? String ?? "0")!
        limit = Int(headers?["x-pagination-limit"] as? String ?? "0")!
    }
}


// MARK: - TMDB Movie Images
class TMDBMovieImagesRequest: Request {
    convenience init(id: Movie.MovieID) {
        self.init()

        api = .tmdb
        path = "movie/\(id.tmdb)/images?api_key=\(api.apiKey)"
    }
}

class TMDBMovieImagesResponse: Response {
    var posterUrl: String = ""
    var backdrops: [MovieImage] = []

    override func map() {
        if let temPosters = jsonFields?.first?["posters"] as? [[String: Any]],
            let first = temPosters.first,
            let top = first["file_path"] as? String {
            posterUrl = top
        }

        guard let temBackdrops = jsonFields?.first?["backdrops"] as? [[String: Any]] else { return }
        temBackdrops.forEach { (backdrop) in
            guard let filePath = backdrop["file_path"] as? String else { return }
            backdrops.append(MovieImage(url: filePath))
        }
    }
}


// MARK: - Movie Extended Info
class MovieExtendedRequest: Request {
    internal var movie: Movie!

    required init(movie: Movie) {
        super.init()

        api = .trakt
        path = "movies/\(movie.id.trakt)?extended=full"
    }
}

class MovieExtendedResponse: Response {
    var extendedMovie: Movie!

    override func map() {
        guard let jsonMovie = jsonFields?.first else { return }
        extendedMovie = Movie(fields: jsonMovie)

        extendedMovie.expanded = true
        extendedMovie.runtime = jsonMovie["runtime"] as? UInt ?? 0
        extendedMovie.tagline = jsonMovie["tagline"] as? String ?? ""
        extendedMovie.overview = jsonMovie["overview"] as? String ?? ""
        extendedMovie.rating = jsonMovie["rating"] as? Double ?? 0
        extendedMovie.genres = jsonMovie["genres"] as? [String] ?? []

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale.current
        let released = jsonMovie["released"] as? String ?? ""
        extendedMovie.releaseDate = formatter.date(from: released)
    }
}
