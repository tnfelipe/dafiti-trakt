//
//  UIColorExtension.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 05/08/17.
//
//

import UIKit

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1) {
        var red:   CGFloat = 0.0, green: CGFloat = 0.0
        var blue:  CGFloat = 0.0, alpha: CGFloat = 1.0

        red = r / 255.0
        green = g / 255.0
        blue = b / 255.0
        alpha = a
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
