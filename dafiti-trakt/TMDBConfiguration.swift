//
//  TMDBConfiguration.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 05/08/17.
//
//

import UIKit

struct TMDBConfiguration {
    var baseUrl: String
    var posterSizes: [String]
    var imagesSizes: [String]

    func smallerPosterSize() -> String {
        guard posterSizes.count > 0, let first = posterSizes.first else { return "" }
        return first
    }

    func preferredImageSize() -> String {
        if imagesSizes.contains("w300") {
            return "w300"
        }

        guard imagesSizes.count > 0, let first = imagesSizes.first else { return "" }
        return first
    }
}
var tmdbConfiguration: TMDBConfiguration? = nil
var tmdbConfigurationIsLoading = false

class TMDBConfigurationRequest: Request {
    required override init() {
        super.init()

        api = .tmdb
        path = "configuration?api_key=" + api.apiKey
    }
}

class TMDBConfigurationResponse: Response {
    open var baseUrl = ""
    open var posterSizes: [String] = []
    open var imagesSizes: [String] = []

    override func map() {
        guard let json = jsonFields?.first else { return }
        guard let jsonImages = json["images"] as? [String: Any] else { return }

        baseUrl = jsonImages["secure_base_url"] as? String ?? ""

        if let posterSizes = jsonImages["poster_sizes"] as? [String] {
            self.posterSizes = posterSizes
        }

        if let imagesSizes = jsonImages["backdrop_sizes"] as? [String] {
            self.imagesSizes = imagesSizes
        }
    }
}
