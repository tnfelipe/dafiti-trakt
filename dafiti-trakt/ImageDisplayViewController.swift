//
//  ImageDisplayViewController.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 05/08/17.
//
//

import UIKit

class ImageDisplayViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var toolBar: UIToolbar!

    open var image: UIImage!
    private var showingToolbars = true

    // MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitle()

        scrollView.delegate = self
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 10

        let tap = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
        imageView.addGestureRecognizer(tap)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = UIColor.white

        guard image != nil else {
            alert(message: "Something went wrong.\nPlease try again.", completionHandler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
            return
        }
        imageView.image = image
    }

    // MARK: - Actions
    internal func tap(_ gesture: UITapGestureRecognizer) {
        showingToolbars = !showingToolbars
        toolBar.isHidden = showingToolbars
        navigationController?.setNavigationBarHidden(showingToolbars, animated: true)
    }

    @IBAction func share(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        present(activityVC, animated: true, completion: nil)
    }
}

extension ImageDisplayViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
