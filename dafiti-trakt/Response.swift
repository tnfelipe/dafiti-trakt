//
//  Response.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 04/08/17.
//
//

import UIKit

class Response {
    open var error: DTError?
    open var isError: Bool = false

    // data
    private var jsonData: Data?
    internal var jsonFields: [[String: Any]]?
    internal var headers: [AnyHashable: Any]?

    required init() {}

    ///
    open func setError(_ error: DTError) {
        isError = true
        self.error = error
    }

    open func setJSONData(_ data: Data?, urlResponse: HTTPURLResponse? = nil) {
        jsonData = data
        guard jsonData != nil else {
            isError = true
            error = DTError.jsonError
            return
        }

        do {
            let any = try JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments)
            jsonFields = Response.arrayrize(any)
        } catch {
            isError = true
            self.error = DTError.jsonError
        }

        if let headers = urlResponse?.allHeaderFields, headers.count > 0 {
            self.headers = [:]
            headers.forEach { self.headers![$0.0] = $0.1 }
        }
    }

    // should be overriden
    open func map() {}

    // arrayrize
    class func arrayrize<T> (_ collection: Any) -> [T] {
        var arr: [T] = []

        if let single = collection as? T {
            arr.append(single)
        } else if let multi = collection as? [T] {
            arr.append(contentsOf: multi)
        }

        return arr
    }
}
