//
//  Client.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 04/08/17.
//
//

import UIKit

class Client {
    private init() {}

    open static func get<Req: Request, Res: Response> (request: Req, recall: Int = 0, completionHandler: @escaping (Res) -> Void) {
        let url = request.getURL()
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: 10)
        request.headers()?.forEach({ (header: String, value: String) in
            urlRequest.addValue(value, forHTTPHeaderField: header)
        })

        urlRequest.httpBody = request.getBody()

        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 10.0
        let session = URLSession(configuration: sessionConfig)

        let task = session.dataTask(with: urlRequest, completionHandler: { (responseData, urlResponse, error) in
            let response = Res()
            guard error == nil else {
                if recall < 3 {
                    // tenta rechamar
                    DispatchQueue.global().asyncAfter(deadline: .now() + 3, execute: {
                        Client.get(request: request, recall: recall + 1, completionHandler: completionHandler)
                    })
                } else {
                    response.setError(DTError.responseError)
                    completionHandler(response)
                }
                return
            }

            response.setJSONData(responseData, urlResponse: urlResponse as? HTTPURLResponse)
            completionHandler(response)
        })
        task.resume()
    }

    open static func getImage(_ imgFilePath: String,
                              withSize size: String = "",
                              completionHandler: ((UIImage?, DTError?) -> Void)? = nil) {

        // needs tmdbConfig to load images, so check that before
        guard tmdbConfiguration != nil else {
            // is it loading? if so, try later
            guard !tmdbConfigurationIsLoading else {
                DispatchQueue.global().asyncAfter(deadline: .now() + 3, execute: {
                    Client.getImage(imgFilePath,
                                    withSize: size,
                                    completionHandler: completionHandler)
                })
                return
            }

            // load tmdbConfig
            tmdbConfigurationIsLoading = true
            TMDBConfigurationRequest().request(completionHandler: { (response: TMDBConfigurationResponse) in
                tmdbConfigurationIsLoading = false
                guard !response.isError else {
                    // will try again?
                    return
                }

                tmdbConfiguration = TMDBConfiguration(
                    baseUrl: response.baseUrl, posterSizes: response.posterSizes, imagesSizes: response.imagesSizes)

                // try to load the image again
                DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
                    Client.getImage(imgFilePath,
                                    withSize: size,
                                    completionHandler: completionHandler)
                })
            })
            return
        }

        let imgSize = size.isEmpty ? tmdbConfiguration!.smallerPosterSize() : size
        let imgUrl = tmdbConfiguration!.baseUrl + imgSize + imgFilePath
        guard let url = URL(string: imgUrl) else {
            completionHandler?(nil, DTError.cantLoadImage)
            return
        }

        let urlRequest = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10)
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 15.0
        let session = URLSession(configuration: sessionConfig)

        let task = session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            guard let imgData = data, error == nil else {
                completionHandler?(nil, DTError.cantLoadImage)
                return
            }

            DispatchQueue.main.async {
                let image = UIImage(data: imgData)
                completionHandler?(image, nil)
            }
        }
        task.resume()
    }
}
