//
//  CatalogViewController.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 05/08/17.
//
//

import UIKit

class CatalogViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!

    fileprivate var isLoading = false
    fileprivate var loadedMovies: [Movie] = []
    fileprivate var popularMovies: PopularMoviesResponse?

    // search
    fileprivate var searching = false {
        didSet {
            //guard oldValue != searching else { return }
            if searching {
                loadMovies(page: 1, query: txtSearch.text!)
            } else {
                loadMovies()
                view.endEditing(true)
                txtSearch.text = ""
                txtSearch.resignFirstResponder()
            }
        }
    }

    // MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitle()

        txtSearch.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        loadMovies()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let btnRefresh = UIBarButtonItem(title: "Refresh", style: .plain, target: self, action: #selector(tappedRefresh))
        btnRefresh.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = btnRefresh

        let btnMore = UIBarButtonItem(title: "Load more", style: .plain, target: self, action: #selector(tappedLoadMore))
        btnMore.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = btnMore

        // always shows 3 items per line, even on old iphones
        if UIScreen.main.bounds.width <= 320 {
            let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            flow.sectionInset = UIEdgeInsets.zero
            let width = UIScreen.main.bounds.width - 6
            flow.itemSize = CGSize(width: width / 3 - 6, height: flow.itemSize.height)
            flow.minimumInteritemSpacing = 3
            flow.minimumLineSpacing = 3
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MovieDetailsViewController {
            guard let selectedRow = collectionView.indexPathsForSelectedItems?.first?.row else { return }
            vc.delegate = self
            vc.movie = loadedMovies[selectedRow]
        }
    }

    // MARK: - Actions
    internal func tappedRefresh() {
        loadMovies()
        searching = false
    }

    internal func tappedLoadMore() {
        loadMovies(page: popularMovies!.page + 1, query: searching ? txtSearch.text! : "")
    }

    fileprivate func loadMovies(page: Int = 1, limit: Int = 10, query: String = "") {
        isLoading = true
        let loadingOverlay = view.showLoading()


        let q = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let popularMoviesRequest = PopularMoviesRequest(page: page, limit: limit, query: q)
        popularMoviesRequest.request { (response: PopularMoviesResponse) in
            DispatchQueue.main.async {
                self.isLoading = false
                loadingOverlay.removeFromSuperlayer()

                self.popularMovies = response
                if response.page > 1 {
                    self.loadedMovies.append(contentsOf: response.movies)
                } else {
                    self.loadedMovies = response.movies
                }
                self.collectionView.reloadData()
            }
        }
    }
}

extension CatalogViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return loadedMovies.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "catalogCell", for: indexPath)
        guard let catalogCell = cell as? CatalogCollectionViewCell else { return cell }

        let movie = loadedMovies[indexPath.row]
        catalogCell.title.text = movie.title
        catalogCell.year.text = movie.year

        movie.getPoster(addedTo: catalogCell.image, completionHandler: nil)
        //catalogCell.image.image = movie.poster?.image; movie.posterView = catalogCell.image
        return catalogCell
    }
}

extension CatalogViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showMovieDetails", sender: self)
    }
}

extension CatalogViewController: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searching = false
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        defer { textField.resignFirstResponder() }

        if let text = textField.text, !text.isEmpty {
            searching = true
        } else {
            searching = false
        }
        return true
    }
}

extension CatalogViewController: MovieDetailsDelegate {
    func updateMovieInfo(_ movie: Movie) {
        guard let selectedRow = collectionView.indexPathsForSelectedItems?.first?.row else { return }
        loadedMovies[selectedRow] = movie
    }
}
