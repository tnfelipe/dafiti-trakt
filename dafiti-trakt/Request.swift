//
//  Request.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 04/08/17.
//
//

import UIKit

class Request {
    enum RequestAPI: UInt {
        case trakt = 0, tmdb

        var baseURL: String {
            switch self {
            case .trakt: return "https://api.trakt.tv/"
            case .tmdb: return "https://api.themoviedb.org/3/"
            }
        }

        var apiKey: String {
            switch self {
            case .trakt: return "2e40b1cc1a2d5388513f258f94f4f021c0d23ace5f49c614cddc9742ac39629f"
            case .tmdb: return "dd1292e3054f2e986ddd3f1caafbe3ba"
            }
        }

        var headers: [String: String]? {
            guard self == .trakt else { return nil }
            return [
                "Content-Type": "application/json",
                "trakt-api-key": apiKey,
                "trakt-api-version": "2"
            ]
        }
    }

    internal var api: RequestAPI!
    internal var path: String!
    internal var reqHeaders: [String: String]?

    private var body: Data?

    open func getURL() -> URL {
        return URL(string: api.baseURL + path)!
    }

    open func headers() -> [String: String]? {
        var requestHeaders = api.headers
        reqHeaders?.forEach { requestHeaders?[$0.0] = $0.1 }
        return requestHeaders
    }

    /// get json data
    open func getBody() -> Data? {
        return body
    }

    open func request<Res: Response> (completionHandler: @escaping(Res) -> Void) {
        Client.get(request: self) { (response: Res) in
            if !response.isError {
                response.map()
            }

            completionHandler(response)
        }
    }
}
