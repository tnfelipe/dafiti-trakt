//
//  DTError.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 04/08/17.
//
//

import UIKit

public enum DTError: Int, Error {
    case unknown = -99

    // web
    case urlError = 100, responseError, jsonError, cantLoadImage
}
