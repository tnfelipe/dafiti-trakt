//
//  UIViewExtension.swift
//  dafiti-trakt
//
//  Created by Felipe Rui on 05/08/17.
//
//

import UIKit

extension UIView {
    func showLoading() -> CAShapeLayer {
        let overlay = CAShapeLayer()
        overlay.frame = layer.frame
        overlay.opacity = 0.7
        overlay.backgroundColor = UIColor.black.cgColor

        let circle = CAShapeLayer()
        circle.frame = CGRect(x: overlay.frame.midX - 20, y: overlay.frame.midY - 20, width: 40, height: 40)
        circle.shouldRasterize = true
        circle.contentsScale = UIScreen.main.scale
        circle.rasterizationScale = UIScreen.main.scale
        circle.backgroundColor = nil
        circle.fillColor = nil//UIColor.red.cgColor
        circle.strokeColor = UIColor(r: 222, g: 18, b: 25).cgColor
        circle.lineWidth = 3
        circle.lineCap = kCALineCapRound

        // circle path
        let path = UIBezierPath()
        path.addArc(
            withCenter: CGPoint(x: circle.frame.size.width / 2, y: circle.frame.size.height / 2), radius: circle.frame.size.width / 2,
            startAngle: -3 * CGFloat.pi / 4, endAngle: -11 * CGFloat.pi / 4, clockwise: false)
        circle.path = path.cgPath
        circle.strokeEnd = 0

        // animations
        let duration: CFTimeInterval = 1.15
        // grow animation
        let growAni = CAKeyframeAnimation(keyPath: "strokeEnd")
        growAni.keyTimes = [0, 0.2, 0.5]
        growAni.values = [0, 0.6, 0.99]

        // erase animation
        let eraseAni = CAKeyframeAnimation(keyPath: "strokeStart")
        eraseAni.keyTimes = [0.51, 0.8, 1]
        eraseAni.values = [0, 0.5, 1]

        // alpha
        let alphaAni = CAKeyframeAnimation(keyPath: "opacity")
        alphaAni.keyTimes = [0.6, 0.85, 1]
        alphaAni.values = [0.9, 0.6, 0.15]

        // animation
        let animations = CAAnimationGroup()
        animations.animations = [growAni, eraseAni, alphaAni]
        animations.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animations.duration = duration
        animations.repeatCount = HUGE
        animations.isRemovedOnCompletion = false

        overlay.addSublayer(circle)
        layer.addSublayer(overlay)

        layer.speed = 1
        circle.add(animations, forKey: "animation")

        return overlay
    }
}
